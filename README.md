# useR! Template
This is a template for the useR! 2020 talks. 

## Usage

Do the following to use this template: 

```
library("devtools")
devtools::install_gitlab("R-conferences/usertemplate")
```
Then, the template can be created within RStudio from the menu:   
*File -> New File -> R Markdown... -> useR_template*

## Example 
An example is shown [example](example/).
